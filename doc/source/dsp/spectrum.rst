Signal Analysis
***************

Aim 
===

In this page, we will measure different characterstic of the signal e.g. Peaks, Bandwidth and Peak to Averate Ratio (PAR) etc. 

Matlab : R2020a 


LTE20 data
==========

* LTE20 data can be downloaded from :download:`here <data/LTE20_30_72_MHz.mat>`.
* To plot the data in Simulink, download the design :download:`LTE20_spectrum.slx <data/LTE20_spectrum.slx>`.
* The design is explained in :numref:`sec_plotting_the_data_in_simulink`
* Results are shown in :numref:`fig_spectrum1`

.. _fig_spectrum1:

.. figure:: img/spectrum1.png

   Specturm of LTE20 data


Peak finder
===========

* To find the peaks in the spectrum, we can use the 'peak finder' button in the spectrum analyzer as shown in :numref:`fig_spectrum2`. 
* In the results, we have used 'Max Num of Peak = 2', therefore only two peaks are shown in the results. 
* Also, we enabled X+Y, therefore both X and Y values are shown in the results for the peak location. 
* Last, we need to tick the boxes, so that values will be displayed on the spectrum as well (otherwise it will displayed on the right side window i.e. spectrum analyzer window). 

.. _fig_spectrum2:

.. figure:: img/spectrum2.png

   Peak finder


Cursor measurement
==================

* :numref:`fig_spectrum3` shows the Cursor measurement option in the spectrum analyzer. The cursors are name as 1 and 2
* It can measure the differences in power and frequencies between two carriers (along with individual power and frequency). 

.. _fig_spectrum3:

.. figure:: img/spectrum3.png

   Cursor measurement


Cumulative Distribution Function measurement (CCDF)
===================================================

* :numref:`fig_spectrum4` shows the CCDF plot of the data.
* Usually, we measure the CCDF at 0.01% probability (or lower) as shown in the 'red box'.  
* In the result, the CCDF is 9.542 dB at probability 0.01% (or 0.0001) i.e. average power which is above the 9.542 has a probability = 0.0001.  

.. _fig_spectrum4:

.. figure:: img/spectrum4.png

   Cursor measurement


