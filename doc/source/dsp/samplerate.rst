Sample rate
***********

Aim 
===

In this page, we will increase and decrease sampling rate of LTE20 data. 
We will see the it's effect on the spectrum.

Matlab : R2020a 


LTE20 data
==========

* LTE20 data can be downloaded from :download:`here <data/LTE20_30_72_MHz.mat>`.
* Lets plot this data first using below code, 

.. code-block:: matlab

    % LTE20_data_generate.m

    clear all;
    close all;
    clc;

    % provide the correct path to .mat file
    LTE20_data_load = load('LTE20_30_72_MHz.mat');
    LTE20_data = LTE20_data_load.data; 

    % Above data was generated at 30.72 MHz
    Fs = 30.72e6;

    % spectrum
    pspectrum(LTE20_data, Fs);

* Results of above code is shown in :numref:`fig_datarate1`

.. _fig_datarate1:

.. figure:: img/datarate1.png

   Specturm of LTE20 data


.. _sec_plotting_the_data_in_simulink:

Plotting the data in Simulink
=============================

* To plot the data in Simulink, download the design :download:`LTE20_spectrum.slx <data/LTE20_spectrum.slx>`.
* And run this design to get the results in :numref:`fig_datarate2`

.. _fig_datarate2:

.. figure:: img/datarate2.png

   Specturm of LTE20 data in Simulink 


* Settings in :numref:`fig_datarate3` and :numref:`fig_datarate4` are used in :numref:`fig_datarate2`. 
* Also, Fs and LTE20_data in :numref:`fig_datarate4` are defined in :numref:`fig_datarate3` in 'InitFcn'. 
* Date is limited samples in LTE20_data i.e. 307200 samples. Therefore, 'cyclic repetition' option is used in :numref:`fig_datarate4`. 


.. _fig_datarate3:

.. figure:: img/datarate3.png

   InitFcn to initialize the values 




.. _fig_datarate4:

.. figure:: img/datarate4.png

   Data generator



Interpolation
=============

* Now, we will add the "interpolation by 2" filter (i.e. x[n/2]) to increase the Fs to 61.44 MHz (i.e. 30.72 `*` 2 MHz) as shown in :numref:`fig_datarate5`. 
* For this, download the Simulink design :download:`LTE20_interpolated.slx <data/LTE20_interpolated.slx>`.



.. _fig_datarate5:

.. figure:: img/datarate5.png

   InitFcn to initialize the values 


* We have two Spectrum Analyzer here which are named as 'interpolated' and 'Two spectrum'. 
* In 'Two spectrum' case, the 'matrix concatenate' block is used to concatenate the signals before and after the interpolation.
* First, we will see the spectrum of  'interpolated' which is shown in :numref:`fig_datarate6`. Fs can be seen at the bottom of the screenshot. 


.. _fig_datarate6:

.. figure:: img/datarate6.png

   Interpolated output : Fs = 61.44


* Spectrum of 'Two spectrum' is shown in :numref:`fig_datarate7`. Here, the span of the Spectrum Analyzer is +/- 30.72 MHz. Therefore we can see the images for Fs = 30.72 MHz (in Yellow color), which are far away from the desired band (i.e. +/- 15.36 MHz i.e. +/- Fs/2). 
* When we interpolate the signal then sample rate becomes double (i.e. 61.44 MHz) and there are no images in range +/- 30.72 (i.e. new +/- Fs/2) as shown in 'blue spectrum' of :numref:`fig_datarate7`.   


.. _fig_datarate7:

.. figure:: img/datarate7.png

   Spectrum of Fs = 61.44 and 30.72 in same window


Decimation
==========

Now, we will add the "decimation by 2" filter (i.e. x[2n]) to decrease the Fs to 30.72 MHz again as shown in :numref:`fig_datarate8`. Results are shown in :numref:`fig_datarate9`


.. _fig_datarate8:

.. figure:: img/datarate8.png

   Decimation filter is added after interpolation


.. _fig_datarate9:

.. figure:: img/datarate9.png

   Spectrum of Fs = 30.72 at input and output



Required Fs
===========

* **For the proper operation Fs/2 should be greater than signal iBW (instantaneous bandwidth)**.
* LTE20 signal, which is centered around 0 MHz, has iBW = 10 MHz; therefore Fs should be greater than 20 MHz. If it is centered at 30 MHz, then iBW will be '30 + 10 = 40 MHz', and required Fs/2 should be greater that 40 MHz. 
* We can not apply decimation filter directly to LTE20 signal which is generated at 30.72 MHz.  Here, Fs/2 (i.e. 7.68 MHz) < iBW (i.e 10 MHz), therefore we will have incorrect output as shown in :numref:`fig_datarate10` . 

Here, the output has the images of the main signal in the desired bandwidth +/-15 MHz (see in Blue); however the expected results is in :numref:`fig_datarate7` where there is no signal in +/-15 MHz rage (shown in Yellow). 
  

.. _fig_datarate10:

.. figure:: img/datarate10.png

   Incorrect output as Fs/2 < iBW


* Also, it will work fine if we use "interpolation by 2" and "decimation by 3", therefore the overall Fs is "30.72 `*` 2 / 3 = 20.48 MHz". Hence, Fs/2 (i.e. 10.24 MHz) > iBW (i.e. 10 MHz) and performance will be good in simulation as shown in :numref:`fig_datarate11`. 
* However, for **practical reasons (i.e. filter order will increase significantly as we have only 0.24 MHz transition band), it is recommended to use Fs = 30.72MHz for LTE20 signal which is cetnered at 0**. 


.. _fig_datarate11:

.. figure:: img/datarate11.png

   "Interpolation by 2", then "decimation by 3"


* We used 30.72 in above point because Fs is usually a multiple of 1.92 in communication system as shown in :numref:`table_LTE_BW`. 


.. _table_LTE_BW:

.. Table:: Specification for LTE signals

    +----------------------+---------------------------+--------------------------+----------+
    | LTE Channel BW (MHz) | Occupied Channel BW (MHz) | Sampling frequency (MHz) | FFT Size |
    +======================+===========================+==========================+==========+
    | 1.25                 | 1.140                     | 1.92                     | 128      |
    +----------------------+---------------------------+--------------------------+----------+
    | 2.5                  | 2.265                     | 3.84                     | 256      |
    +----------------------+---------------------------+--------------------------+----------+
    | 5                    | 4.515                     | 7.68                     | 512      |
    +----------------------+---------------------------+--------------------------+----------+
    | 10                   | 9.015                     | 15.36                    | 1024     |
    +----------------------+---------------------------+--------------------------+----------+
    | 15                   | 13.515                    | 23.04                    | 1536     |
    +----------------------+---------------------------+--------------------------+----------+
    | 20                   | 18.015                    | 30.72                    | 2048     |
    +----------------------+---------------------------+--------------------------+----------+



* Further, we can't apply "decimation by 3" first and then "interpolation by 2" next (for this particular LTE20 data with Fs=30.72 MHz). As 'decimation by 3' will result in Fs/2 < iBW (i.e. signal will be distorted now). :numref:`fig_datarate12` shows the result for this case, where the iBW of the signal is changed to 6 MHz due to the incorrect sequence of operations. 
  

.. _fig_datarate12:

.. figure:: img/datarate12.png

   "Decimation by 3", then "Interpolation by 2" results in incorrect iBW


Summary
=======

In this page, we used the LTE20 data to see the effect of decimation and interpolation filter. 