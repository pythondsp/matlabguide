filterDesigner and fvtool
*************************

Aim
===

In this page, we will use the filterDesigner (or fdatool) to generate the coefficients for FIR filter. Also, we will perform some filtering operations using these coefficients. 

Matlab : R2020a 


Generate coefficients
=====================

* Launch the fdatool by typing filterDesigner or fdatool in the MATLAB console as shown below, 
  
.. code-block:: matlab

    filterDesigner

* The tool will open as shown in :numref:`fig_fdatool1`.   


.. _fig_fdatool1:

.. figure:: img/fdatool1.png

   filterDesigner tool


* Now, we will generate the coefficients for below settings, 

    - Filter = Low Pass Filter
    - Passband = 20 MHz
    - Passband gain = 1 dB
    - Stopband = 25 MHz
    - Stopband attenuation = -80 dB
    - Sampling frequency (Fs) = 200 MHz 
    - Order = Minimum order 
    
* The settings are shown in :numref:`fig_fdatool2`.   


.. _fig_fdatool2:

.. figure:: img/fdatool2.png

   Settings for above configurations 


* We can click on the"Filter coefficients option" to see the generated coefficients, as shown in :numref:`fig_fdatool3`.   


.. _fig_fdatool3:

.. figure:: img/fdatool3.png

   Generated coefficients


* Let's export these coefficients in variable . For this, go to Files -> Export (or press ctrl+E) -> export with desired name (e.g. LPF_20MHz) as shown in :numref:`fig_fdatool4`.   


.. _fig_fdatool4:

.. figure:: img/fdatool4.png

   Export coefficients


* The coefficients will be available in MATLAB variable list as shown below. We might need to use 'format long' to see the complete coefficients.  

.. code-block:: matlab 

    % Display first 10 coefficients
    >> LPF_20MHz(1:10)

    ans =

       -0.0002   -0.0005   -0.0008   -0.0010   -0.0008    0.0000    0.0016    0.0039    0.0063    0.0082

    % format long - to see more precision 
    >> format long
    >> LPF_20MHz(1:10)

    ans =

      Columns 1 through 9

      -0.000219289468611  -0.000490033861708  -0.000824439948466  -0.001026379855619  -0.000834895235541   0.000006096513892   0.001618772983133   0.003867150209496   0.006302952301528

      Column 10

       0.008234183872402

fvtool
======

* We can verify the coefficients using fvtool. 
* Since we already save the coefficients in variable LPF_20MHz, therefore we can load it using fvtool as shown below, 

.. code-block:: matlab 

    fvtool(LPF_20MHz, 1)


* We can see the results which shown in :numref:`fig_fdatool5`. The results have normalized frequencies.   


.. _fig_fdatool5:

.. figure:: img/fdatool5.png

   Normalized Filter response of coefficients

* We generated the coefficients for Fs=200 MHz. Let's change the Fs to 200 MHz in the plot. For this go to "Analysis -> Sampling frequency -> enter the Fs as 200 MHz" as shown in :numref:`fig_fdatool6`. We can see that the cutoff frequency (or passband) is 20 MHz here.   


.. _fig_fdatool6:

.. figure:: img/fdatool6.png

   Filter response for Fs = 200 MHz


* Let's put the Fs = 400 MHz, see the effect of **same coefficients**. Now, in :numref:`fig_fdatool7`, we can observe the increase in passband (i.e. 40 MHz instead of 20 MHz). 
* In the other words, if we put the incorrect sampling frequency then we will get the undesired response from the same coefficients e.g. LFP will pass upto 40 MHz (instead of 20 MHz) if we use Fs=400 (instead of 200 MHz). 
* Therefore, we should generate and use the coefficients for the same sampling frequency. 

.. _fig_fdatool7:

.. figure:: img/fdatool7.png

   Filter response for Fs = 400 MHz for same coefficients


Simulink design
===============

* Now, we will create a simulink design where two sine waves will be added i.e. 15 MHz and 40 MHz. And then we will use the above coefficients to filter the 15 MHz sine wave. 

* Let's create the design which is shown in :numref:`fig_fdatool12`. 
* The settings of the blocks are shown in :numref:`fig_fdatool8` and :numref:`fig_fdatool9`.
* Here, two sine waves (15 MHz and 30 MHz) are added together and then passed to LPF
* The design can be downloaded from :download:`here <data/LTE20_spectrum.slx>`.
* Input and outputs of the designs are shown in :numref:`fig_fdatool10`

.. _fig_fdatool12:

.. figure:: img/fdatool12.png

   Sum of Sine wave filtered with LPF

.. _fig_fdatool8:

.. figure:: img/fdatool8.png

   Sine wave 15 MHz (modify the 30 MHz similarly)

.. _fig_fdatool9:

.. figure:: img/fdatool9.png

   FIR setting


.. _fig_fdatool10:

.. figure:: img/fdatool10.png

   Output of FIR is 15 MHz signal

* Let's see the effect of incorrect Fs which is mentioned in previous section. Modify the Fs of FIR to 400 MHz (see :numref:`fig_fdatool11`). In this case, we will have cutoff frequency as 40 MHz (see :numref:`fig_fdatool7` for details). Therefore we will get both 15 and 30 MHz signal at the output of FIR as shown in :numref:`fig_fdatool11`. 


.. _fig_fdatool11:

.. figure:: img/fdatool11.png

   Incorrect Fs : FIR output has both 15 and 30 MHz


Summary
=======

In this page, we used the fdatool (filterDesigner) and fvtool to generate and view the coefficients respectively. Also, we saw the effect of incorrect Fs on the behavior of the FIR filter. 