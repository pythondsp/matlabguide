Subsystem and Masking
*********************

Aim
===

Matlab : R2020a 

In this page, we will use the 'masking' feature of MALTAB to load the values to Simulink blocks. We will create below example by combining previous two pages, 

* Generate 3 sine waves of different frequencies using 'mask option' with Fs = 100 MHz; and add them together. We will put these blocks in a subsystem. 
* Next, generate the FIR coefficients for Low Pass, High Pass and Bandpass signal; and load it using mask. We will use Fs =  200 MHz and store fixed coefficient. Also, we will load coefficients using radio button option in the mask.  
* Since, Fs is high for FIR than sine waves, therefore we need to interpolate the sine-data by 2, to increase the Fs to 200 MHz. 



Complete design
===============

* The complete design, which is shown in :numref:`fig_mask4`, can be downloaded from :download:`here <data/mask_example.slx>`.

* In the next sections, we will create it step by step. 


Subsystem
=========

* Subsystem can be useful to spilt the design in smaller blocks. 
* In this section, we will create a subsystem which will add three sine waves of different frequencies. 

* First, create a block design as shown in :numref:`fig_mask8`. 

.. _fig_mask8:

.. figure:: img/mask8.png

   Sum of sine wave of frequencies 15, 30 and 40 MHz


* Next, select all blocks (except spectrum analyzer) and create subsystem as shown in :numref:`fig_mask9`. A subsystem will be created, rename that subsystem to 'Sum of Sine' and run the simulation as shown in :numref:`fig_mask10`. 

.. _fig_mask9:

.. figure:: img/mask9.png

   Create Subsystem


.. _fig_mask10:

.. figure:: img/mask10.png

   Rename subsystem


Mask
====

* Mask can be used to set the parameter from the top level block. This can applied to subsystem or individual blocks. This is a very useful feature which we will see in this section. 
* We will apply mask to subsystem in the previous section. Process is same for individual blocks as well. 

* Right click the subsystem and select 'Mask -> Create Mask' (or select subsystem and press ctrl+M). A subsystem window will be open as shown in :numref:`fig_mask11`
  
.. _fig_mask11:

.. figure:: img/mask11.png

   Mask window

* Now, go to "parameter and dialog" tab. Click on edit (four times) to add 4 parameters as shown in :numref:`fig_mask12`. 
* Please set a value for each parameter as shown in the figure. 
  
.. _fig_mask12:

.. figure:: img/mask12.png

   Add parameters

* This process is called 'masking'. If everything is done correctly so far, then we can see the values as shown in :numref:`fig_mask13`
  
.. _fig_mask13:

.. figure:: img/mask13.png

   See the values which are set in the Masking tab

* Select the block (or mask) and press ctrl+m to modify the mask, if required. 
* Click on the down arrow in the mask to go inside it. 
* Now, we can define the 'variable name' for frequencies and sample period (instead of hard code value) as shown in :numref:`fig_mask14`. 
  
.. _fig_mask14:

.. figure:: img/mask14.png

   Replace fix value with variable

* In this way, we can modify the common parameters (e.g. sample period) from as single place using mask. Similarly, we can add desired parameters in the mask (e.g. frequencies of each sine wave block), so that we need to go to individual blocks for the changes. 
* We can try to change the frequencies and see the results in the spectrum (i.e. double click the mask and modify the frequencies and observer the changes in the spectrum). :numref:`fig_mask15` shows the results after modifying the frequencies using mask. 
  
.. _fig_mask15:

.. figure:: img/mask15.png

   Replace fix value with variable

* Revert back the parameters which are shown in :numref:`fig_mask13`. 

FIR filter subsystem
====================

* In this section, try to create the below subsystem with mask. We can see the attached design, if there are issues while creating it. Below are the quick steps to create it. 
* First, add the three discrete FIR filter, one constant block and one Multiport switch as shown in :numref:`fig_mask16`. Multiport switch settings are shown in :numref:`fig_mask18`. This port will pass the signal at input {1, 2, 3} to output port based on the coeff value. The 'coeff' will get the value as '1, 2 or 3' which will be defined using radio-button-mask. 
* See :numref:`fig_mask1`, :numref:`fig_mask2` and :numref:`fig_mask3` for FIR coefficients. Export the coefficients as variable. In the MALTAB, type the name of the variable with transpose option as below. Copy all the coefficients and paste it in the coefficients tabs of :numref:`fig_mask16`. **We need to paste with transpose option e.g. [1 2 3]'**. 

.. code-block:: matlab 
 
     >> Num'

     ans =

        0.000341053431075
       -0.003631442975045
        0.009769248630868
       -0.010040111147694
        0.000339671290319
        0.005638693397929

.. _fig_mask16:

.. figure:: img/mask16.png

   Replace fix value with variable

.. _fig_mask18:

.. figure:: img/mask18.png

   Multiport switch settings 


.. _fig_mask2:

.. figure:: img/mask2.png

   Low pass filter setting


.. _fig_mask3:

.. figure:: img/mask3.png

   High pass filter setting


.. _fig_mask1:

.. figure:: img/mask1.png

   Bandpass filter setting 

* Create a subsystem for it. We will see the error for the constant block, we can skip the error. 
* Now add the mask to subsystem as shown in :numref:`fig_mask17`. After adding the mask the error for the constant block will be removed (as now we have defined the variable coeff which was used in the subsystem). This radio-button-mask will generate the value 1, 2 and 3 for the option 1, 2 and 3 respectively. This value is connect to Multiport switch which will forward the correct input based on the value (see :numref:`fig_mask16`). 

.. _fig_mask17:

.. figure:: img/mask17.png

   Replace fix value with variable

* Now complete the connection as shown in :numref:`fig_mask4`. 'Interpolator' block is used here as our input sample rate is 100 MHz where the coefficients are generated as 200 MHz. 


.. _fig_mask4:

.. figure:: img/mask4.png

   Complete design

* Run the simulation as shown in :numref:`fig_mask5`, :numref:`fig_mask6` and :numref:`fig_mask7`
  
.. _fig_mask5:

.. figure:: img/mask5.png

   Low pass filter (LPF)

.. _fig_mask6:

.. figure:: img/mask6.png

   High pass filter (HPF)

.. _fig_mask7:

.. figure:: img/mask7.png

   Bandpass filter (BPF)


Summary
=======

In this page, we have seen the example of subsystem and mask. We have create a mask which can perform LPF, BPF and HPF operation based on the value which is selected in the mask. 