Digital signal processing
=========================

In this section, we will see different DSP operations e.g. filtering, up-sample, down-sample and FFT operations etc. 

.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: Contents:

   filterdesigner
   samplerate
   spectrum
   masking