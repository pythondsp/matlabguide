Convolution encoder and Viterbi decoder
***************************************

Aim
===

Convolution codes are the error correction codes which can be applied to input data. In this page, we will use the "Convolution encoder" and "Viterbi decoder (both hard and soft)" in the design. Hard decoder needs the binary data as input; whereas soft decoder can take any fractional value between 0 and 1 as input. 

In :numref:`fig_commtool6` we saw the BER performance of QAM64 in AGWN channel. BER was less than 0.01 at SNR=10dB which is not of practical usage. By adding the error correcting codes, the performance can be improved significantly as shown in :numref:`fig_viterbi2` where BER is 0.00005 at SNR=10 for QAM64 signal. 

Matlab : R2020a 

Viterbi hard decoder with QPSK
==============================

* Below code simulates the BER performance of QPSK system for Convolution encoder with Viterbi decoder. The results are shown in :numref:`fig_viterbi1`. 
* The highlighted part are the code related to Convolution encoder and Viterbi decoder. 
* Viterbi hard-decoder is used here which needs the binary input to decode the data i.e. hDec = comm.ViterbiDecoder('InputFormat','Hard');

.. code-block:: matlab
    :linenos:
    :emphasize-lines: 27-37, 62-70, 12, 78, 81

    close all;
    clear all;
    clc;

    max_num_bit = 1e7; % max number of bits to check before end of simulation
    max_num_error = 1000;% max error to check before end of simulation


    % EbNo values to calculate BER
    EbNo_range = 0:0.5:4;
    BER=zeros(1, length(EbNo_range)); % empty matrix to store BER
    ber_encoded=zeros(1, length(EbNo_range)); % empty matrix to store BER



    % System model
    % Modulator
    qpsk_modulator = comm.QPSKModulator('BitInput', true); % constellation = 00, 01, 10, 11
    % AWGN noise
    AWGN = comm.AWGNChannel;
    % Demodulator
    qpsk_demodulator = comm.QPSKDemodulator('BitOutput', true);
    % error rate
    error_rate = comm.ErrorRate;


    % convolution encoder and decoder
    hConEnc = comm.ConvolutionalEncoder;
    hDec = comm.ViterbiDecoder('InputFormat','Hard');
    % 34 is the default TracebackDepth in the 'comm.ViterbiDecoder' 
    error_rate_encoded = comm.ErrorRate('ReceiveDelay', 34); % calculation of 34 below
    % this should be run on MATLAB console after running the simulation 
    % plot(xcorr(s, r))  %% peak location 1024
    % plot(xcorr(s, r_encoded)) % peak location 990
    % 1024-990
    % % ans =
    %      34


    for i = 1:length(EbNo_range)

        % received signal after AWGN channel
        AWGN.EbNo = EbNo_range(i) + 10*log10(2); % SNR

        num_err = 0; num_bits = 0; error_output=zeros(3,1); % initialize with 0
        num_err_encoded = 0; num_bits_encoded = 0; error_output_encoded=zeros(3,1); % initialize with 0

        bits_per_frame = 1024; % Frame size = n * bits/symbol where  n=integer
        while((num_err_encoded < max_num_error) && (num_bits < max_num_bit))
            % generate tx signal
            s = randi([0 1], bits_per_frame, 1); %  'bits_per_frame' samples : column vector of 0 and 1
            
            % no encoding
            tx_signal = qpsk_modulator.step(s); % generate tx signal                
            rx_signal = AWGN.step(tx_signal); % add AWGN
            r = qpsk_demodulator.step(rx_signal); % demodulate signal
            % error_output has values [BER, number_errors, total_bit]
            error_output = error_rate.step(s, r); % calculate error
            num_err = error_output(2); % it's cumulative (as reset is not used for instance)
            num_bits  = error_output(3); % it's cumulative (as reset is not used for instance)
            
            % encoded data
            encodedData = hConEnc.step(s); % encoded data
            tx_signal_encoded = qpsk_modulator.step(encodedData); % generate tx signal
            rx_signal_encoded = AWGN.step(tx_signal_encoded); % add AWGN
            rx_encoded = qpsk_demodulator.step(rx_signal_encoded); % demodulate signal
            r_encoded = hDec.step(rx_encoded);
            error_output_encoded = error_rate_encoded.step(s, r_encoded); % calculate error
            num_err_encoded = error_output_encoded(2); % it's cumulative (as reset is not used for instance)
            num_bits_encoded  = error_output_encoded(3); % it's cumulative (as reset is not used for instance)



        end

        clc;
        BER(i) = error_output(1) % store the BER values
        ber_encoded(i) = error_output_encoded(1) % store the BER values

        reset(error_rate); % reset the instance of comm.ErrorRate
        reset(error_rate_encoded);
    end


    figure
    % theoretical results
    EbNo_linear = 10.^(EbNo_range/10);
    ber_theory = 0.5*erfc(sqrt(EbNo_linear));
    semilogy(EbNo_range, ber_theory, '*r'); % theory
    hold on
    semilogy(EbNo_range, BER, '--b'); % simulation
    semilogy(EbNo_range, ber_encoded, '--^k'); % encoder
    xlabel('Eb/No (dB)');
    ylabel('BER');
    legend('Theory: no encoding', 'Simuatition: no encoding', 'Simulation: encoded data');
    title('QPSK - BER vs EbNo');
    grid on

.. _fig_viterbi1:

.. figure:: img_commtoolbox/viterbi1.png

   QPSK with Viterbi Decoder


Hard decoder with QAM64
=======================

* Below code simulates the BER performance of QAM64 for Convolution encoder with Viterbi decoder. The results are shown in :numref:`fig_viterbi2`. Code is same as previous code with some name changes for QAM64 modulation. 


.. code-block:: matlab
    :linenos: 

    close all;
    clear all;
    clc;

    max_num_bit = 1e7; % max number of bits to check before end of simulation
    max_num_error = 1000;% max error to check before end of simulation


    % EbNo values to calculate BER
    EbNo_range = 1:10;
    BER=zeros(1, length(EbNo_range)); % empty matrix to store BER
    ber_encoded=zeros(1, length(EbNo_range)); % empty matrix to store BER



    % System model
    % Modulator
    QAM64_modulator = comm.RectangularQAMModulator(64, 'BitInput', true, 'NormalizationMethod', 'Average power');
    % AWGN noise
    AWGN = comm.AWGNChannel;
    % Demodulator
    QAM64_demodulator = comm.RectangularQAMDemodulator(64, 'BitOutput', true, 'NormalizationMethod', 'Average power');
    % error rate
    error_rate = comm.ErrorRate;


    % convolution encoder and decoder
    hConEnc = comm.ConvolutionalEncoder;
    hDec = comm.ViterbiDecoder('InputFormat','Hard');
    % 34 is the default TracebackDepth in the 'comm.ViterbiDecoder' 
    error_rate_encoded = comm.ErrorRate('ReceiveDelay', 34); % calculation of 34 below
    % this should be run on MATLAB console after running the simulation 
    % plot(xcorr(s, r))  %% peak location 1024
    % plot(xcorr(s, r_encoded)) % peak location 990
    % 1024-990
    % % ans =
    %      34


    for i = 1:length(EbNo_range)

        % received signal after AWGN channel
        AWGN.EbNo = EbNo_range(i) + 10*log10(6); % SNR
        num_err = 0; num_bits = 0; error_output=zeros(3,1); % initialize with 0
        num_err_encoded = 0; num_bits_encoded = 0; error_output_encoded=zeros(3,1); % initialize with 0

        bits_per_frame = 1026; % Frame size = n * bits/symbol where  n=integer
        while((num_err_encoded < max_num_error) && (num_bits < max_num_bit))
            % generate tx signal
            s = randi([0 1], bits_per_frame, 1); %  'bits_per_frame' samples : column vector of 0 and 1
            
            % no encoding
            tx_signal = QAM64_modulator.step(s); % generate tx signal                
            rx_signal = AWGN.step(tx_signal); % add AWGN
            r = QAM64_demodulator.step(rx_signal); % demodulate signal
            % error_output has values [BER, number_errors, total_bit]
            error_output = error_rate.step(s, r); % calculate error
            num_err = error_output(2); % it's cumulative (as reset is not used for instance)
            num_bits  = error_output(3); % it's cumulative (as reset is not used for instance)
            
            % encoded data
            encodedData = hConEnc.step(s); % encoded data
            tx_signal_encoded = QAM64_modulator.step(encodedData); % generate tx signal
            rx_signal_encoded = AWGN.step(tx_signal_encoded); % add AWGN
            rx_encoded = QAM64_demodulator.step(rx_signal_encoded); % demodulate signal
            r_encoded = hDec.step(rx_encoded);
            error_output_encoded = error_rate_encoded.step(s, r_encoded); % calculate error
            num_err_encoded = error_output_encoded(2); % it's cumulative (as reset is not used for instance)
            num_bits_encoded  = error_output_encoded(3); % it's cumulative (as reset is not used for instance)



        end

        clc;
        BER(i) = error_output(1) % store the BER values
        ber_encoded(i) = error_output_encoded(1) % store the BER values

        reset(error_rate); % reset the instance of comm.ErrorRate
        reset(error_rate_encoded);
    end


    figure
    % theoretical results
    EbNo_linear = 10.^(EbNo_range/10);
    M=64; k=log2(M);
    ber_theory=(2/k)*(1-1/sqrt(M))*erfc(sqrt(3*k*EbNo_linear/2/(M-1)));
    semilogy(EbNo_range, ber_theory, '*r'); % theory
    hold on
    semilogy(EbNo_range, BER, '--b'); % simulation
    semilogy(EbNo_range, ber_encoded, '--^k'); % encoder
    xlabel('Eb/No (dB)');
    ylabel('BER');
    legend('Theory: no encoding', 'Simuatition: no encoding', 'Simulation: encoded data');
    title('QAM64 - BER vs EbNo');
    grid on

.. _fig_viterbi2:

.. figure:: img_commtoolbox/viterbi2.png

   QAM64 with Viterbi Hard Decoder

Viterbi soft decoder with QAM64
===============================

* In this section, Viterbi soft decoder is used and results are compared with Hard Decoder, 

.. code-block:: matlab
    :linenos:
    :emphasize-lines: 13, 24, 33, 36, 50, 53, 57, 83-91, 98, 102

    close all;
    clear all;
    clc;

    max_num_bit = 1e8; % max number of bits to check before end of simulation
    max_num_error = 100000;% max error to check before end of simulation


    % EbNo values to calculate BER
    EbNo_range = 0:1:10;
    BER=zeros(1, length(EbNo_range)); % empty matrix to store BER
    ber_encoded=zeros(1, length(EbNo_range)); % empty matrix to store BER
    ber_encoded_soft=zeros(1, length(EbNo_range)); % empty matrix to store BER



    % System model
    % Modulator
    QAM64_modulator = comm.RectangularQAMModulator(64, 'BitInput', true, 'NormalizationMethod', 'Average power');
    % AWGN noise
    AWGN = comm.AWGNChannel;
    % Demodulator
    QAM64_demodulator = comm.RectangularQAMDemodulator(64, 'BitOutput', true, 'NormalizationMethod', 'Average power');
    QAM64_demodulator_soft = comm.RectangularQAMDemodulator(64, 'BitOutput', true, 'NormalizationMethod', 'Average power',...
        'DecisionMethod', 'Approximate log-likelihood ratio');
    % error rate
    error_rate = comm.ErrorRate;


    % convoution encoder and decoder
    hConEnc = comm.ConvolutionalEncoder;
    hDec_hard = comm.ViterbiDecoder('InputFormat','Hard');
    hDec_soft = comm.ViterbiDecoder('InputFormat','Unquantized');
    % 34 is the default TracebackDepth in the 'comm.ViterbiDecoder'
    error_rate_encoded = comm.ErrorRate('ReceiveDelay', 34); % calculation of 34 below
    error_rate_encoded_soft = comm.ErrorRate('ReceiveDelay', 34); % calculation of 34 below

    % this should be run on MATLAB console after running the simulation
    % plot(xcorr(s, r))  %% peak location 1024
    % plot(xcorr(s, r_encoded)) % peak location 990
    % 1024-990
    % % ans =
    %      34


    for i = 1:length(EbNo_range)

        % received signal after AWGN channel
        snr_db = EbNo_range(i) + 10*log10(6); % SNR
        noiseVar = 10.^(-snr_db/10);
        
        AWGN.EbNo  = snr_db;
        QAM64_demodulator_soft.Variance = noiseVar; %noiseVar;
        
        num_err = 0; num_bits = 0; error_output=zeros(3,1); % initialize with 0
        num_err_encoded = 0; num_bits_encoded = 0; error_output_encoded=zeros(3,1); % initialize with 0
        num_err_encoded_soft = 0; num_bits_encoded_soft = 0; error_output_encoded_soft=zeros(3,1); % initialize with 0

        bits_per_frame = 1026; % Frame size = n * bits/symbol where  n=integer
        while((num_err < max_num_error) && (num_bits < max_num_bit))
            % generate tx signal
            s = randi([0 1], bits_per_frame, 1); %  'bits_per_frame' samples : column vector of 0 and 1

            % no encoding
            tx_signal = QAM64_modulator.step(s); % generate tx signal
            rx_signal = AWGN.step(tx_signal); % add AWGN
            r = QAM64_demodulator.step(rx_signal); % demodulate signal
            % error_output has values [BER, number_errors, total_bit]
            error_output = error_rate.step(s, r); % calculate error
            num_err = error_output(2); % it's cumulative (as reset is not used for instance)
            num_bits  = error_output(3); % it's cumulative (as reset is not used for instance)

            % encoded data : Hard decoder
            encodedData = hConEnc.step(s); % encoded data
            tx_signal_encoded = QAM64_modulator.step(encodedData); % generate tx signal
            rx_signal_encoded = AWGN.step(tx_signal_encoded); % add AWGN
            rx_encoded = QAM64_demodulator.step(rx_signal_encoded); % demodulate signal
            r_encoded = hDec_hard.step(rx_encoded);
            error_output_encoded = error_rate_encoded.step(s, r_encoded); % calculate error
            num_err_encoded = error_output_encoded(2); % it's cumulative (as reset is not used for instance)
            num_bits_encoded  = error_output_encoded(3); % it's cumulative (as reset is not used for instance)
            
            % encoded data : Soft decoder
            encodedData_soft = hConEnc.step(s); % encoded data
            tx_signal_encoded_soft = QAM64_modulator.step(encodedData); % generate tx signal
            rx_signal_encoded_soft = AWGN.step(tx_signal_encoded_soft); % add AWGN
            rx_encoded_soft = QAM64_demodulator_soft.step(rx_signal_encoded_soft); % demodulate signal
            r_encoded_soft = hDec_soft.step(rx_encoded_soft);
            error_output_encoded_soft = error_rate_encoded_soft.step(s, r_encoded_soft); % calculate error
            num_err_encoded_soft = error_output_encoded_soft(2); % it's cumulative (as reset is not used for instance)
            num_bits_encoded_soft  = error_output_encoded_soft(3); % it's cumulative (as reset is not used for instance)

        end

        clc;
        BER(i) = error_output(1) % store the BER values
        ber_encoded(i) = error_output_encoded(1) % store the BER values
        ber_encoded_soft(i) = error_output_encoded_soft(1) % store the BER values

        reset(error_rate); % reset the instance of comm.ErrorRate
        reset(error_rate_encoded);
        reset(error_rate_encoded_soft);

    end


    figure
    % theoretical results
    EbNo_linear = 10.^(EbNo_range/10);
    M=64; k=log2(M);
    ber_theory=(2/k)*(1-1/sqrt(M))*erfc(sqrt(3*k*EbNo_linear/2/(M-1)));
    semilogy(EbNo_range, ber_theory, '*r'); % theory
    hold on
    semilogy(EbNo_range, BER, '--b'); % simulation
    semilogy(EbNo_range, ber_encoded, '--^k'); % encoder
    semilogy(EbNo_range, ber_encoded_soft, '--oc'); % encoder

    xlabel('Eb/No (dB)');
    ylabel('BER');
    legend('Theory: no encoding', 'Simuatition: no encoding', ... 
        'Simulation: encoded data - Hard','Simulation: encoded data - Soft');
    title('QAM64 - BER vs EbNo');
    grid on



* Results are shown in :numref:`fig_viterbi3`. We can see that soft decoder performs better than the hard decoder. 

.. _fig_viterbi3:

.. figure:: img_commtoolbox/viterbi3.png

   QAM64 with Viterbi Soft Decoder


Summary
=======

In this section, we used the Convolution encoder and Viterbi decoder to see the improvement in the BER performance. Also, we saw that soft decoder perform better than the hard decoder. 
