Signal generation and BER plot
******************************

Aim
===

In this page, we will generate some basic signals e.g. QPSK, QAM and AWGN. Also, we will create a basic communication system i.e. modulate the signal, transmit it from the noisy channel, and then demodulated it. Also, we will compare the theoretical BER with simulation BER. 

Matlab : R2020a 


Generate QPSK signal
====================

* In this section, we will generate the QPSK signal using Communication toolbox.
* First, we will create the 'instance' of the object 'QPSKModulator' as shown in below code. Here  

.. code-block:: matlab
    :linenos:

    close all;
    clear all; 
    clc;

    qpsk_modulator = comm.QPSKModulator(); % constellation = 0, 1, 2, 3
    constellation(qpsk_modulator)  % plot the constellation
    w = constellation(qpsk_modulator) % reference constellation points


    qpsk_modulator = comm.QPSKModulator('BitInput', true); % constellation = 00, 01, 10, 11
    constellation(qpsk_modulator) % plot the constellation
    b = constellation(qpsk_modulator) % reference constellation points

* The result of above code is shown in :numref:`fig_commtool1`.   


.. _fig_commtool1:

.. figure:: img_commtoolbox/commtool1.png

   constellation with BitInput = "True" and "False(default)"


* To generate the data, we need to use the 'step' function as shown below. See the comments for details, 
  
.. code-block:: matlab
    :linenos:

    close all;
    clear all; 
    clc;

    qpsk_modulator = comm.QPSKModulator(); % constellation = 0, 1, 2, 3
    % values should be 0, 1, 2 and 3 (i.e. above constellation)
    s = [0; 1; 2; 1; 3; 2; 1];
    signal = qpsk_modulator.step(s)

    qpsk_modulator = comm.QPSKModulator('BitInput', true); % constellation = 00, 01, 10, 11
    % each two bit will make a pair therefore 
    % below is equivalent to s = [0; 1; 2; 1; 3; 2; 1];
    sb = [0; 0; 0; 1; 1; 0; 0; 1; 1; 1; 1; 0; 0; 1];
    signal_bitinput = qpsk_modulator.step(sb)


* Generated data. Here 0 -> 0.7071 and 1-> -0.7071. Therefore 00 = 0.7071 + 0.7071i; and 10 = -0.7071 + 0.7071i etc. 

.. code-block:: text

    signal =

       0.7071 + 0.7071i
      -0.7071 + 0.7071i
       0.7071 - 0.7071i
      -0.7071 + 0.7071i
      -0.7071 - 0.7071i
       0.7071 - 0.7071i
      -0.7071 + 0.7071i


    signal_bitinput =

       0.7071 + 0.7071i
      -0.7071 + 0.7071i
       0.7071 - 0.7071i
      -0.7071 + 0.7071i
      -0.7071 - 0.7071i
       0.7071 - 0.7071i
      -0.7071 + 0.7071i


Demodulate the QPSK signal
==========================

* In this section, we will add the AWGN noise to QPSK signal. 
* Next, we demodulate the signal.  
* Finally, we will check the number of bits which are demodulated incorrectly. 

* Below is the code which modulates the signal, then add the AWGN to it. Next, it demodulate and calculate the number of bits in error. 

.. code-block:: matlab
    :linenos:

    close all;
    clear all; 
    clc;

    num_bits = 100000; % total number of bits
    EbNo = 5; 

    % Modulator 
    qpsk_modulator = comm.QPSKModulator('BitInput', true); % constellation = 00, 01, 10, 11
    % generate tx signal 
    s = randi([0 1], num_bits, 1); %  column vector of 0 and 1
    tx_signal = qpsk_modulator.step(s);


    % AWGN noise
    AWGN = comm.AWGNChannel; 
    % received signal after AWGN channel
    AWGN.EbNo = EbNo + 10*log10(2); % SNR  
    rx_signal = AWGN.step(tx_signal); 

    % Demodulator 
    qpsk_demodulator = comm.QPSKDemodulator('BitOutput', true); 
    r = qpsk_demodulator.step(rx_signal); 

    % BER 
    error_rate = comm.ErrorRate; 
    % error_output = [BER, number_errors, total_bit]
    error_output = error_rate.step(s, r);

    fprintf('EbNo = %d \nError/Total_bits = %d/%d = %f\n',EbNo, error_output(2), error_output(3), error_output(1));



* Output of above code (value may varied slightly on each run), 

.. code-block:: text

    EbNo = 5 
    Error/Total_bits = 620/100000 = 0.006200


* Change the EbNo = 5 to EbNo = 10, then we will have below output (i.e. error is decreased as SNR is increased) 

.. code-block:: text

    EbNo = 10 
    Error/Total_bits = 1/100000 = 0.000010


* We can plot the constellation of the received signal as shonw in below code (see last 3 lines of the code), 

.. code-block:: matlab
    :linenos:
    :emphasize-lines: 33-35

    close all;
    clear all; 
    clc;

    num_bits = 100000; % total number of bits
    EbNo = 10; 

    % Modulator 
    qpsk_modulator = comm.QPSKModulator('BitInput', true); % constellation = 00, 01, 10, 11
    % generate tx signal 
    s = randi([0 1], num_bits, 1); %  column vector of 0 and 1
    tx_signal = qpsk_modulator.step(s);


    % AWGN noise
    AWGN = comm.AWGNChannel; 
    % received signal after AWGN channel
    AWGN.EbNo = EbNo + 10*log10(2); % SNR  
    rx_signal = AWGN.step(tx_signal); 

    % Demodulator 
    qpsk_demodulator = comm.QPSKDemodulator('BitOutput', true); 
    r = qpsk_demodulator.step(rx_signal); 

    % BER 
    error_rate = comm.ErrorRate; 
    % error_output = [BER, number_errors, total_bit]
    error_output = error_rate.step(s, r);

    fprintf('EbNo = %d \nError/Total_bits = %d/%d = %f\n',EbNo, error_output(2), error_output(3), error_output(1));

        
    % constellation diagram 
    constDiagram = comm.ConstellationDiagram('SamplesPerSymbol', 2);
    constDiagram(rx_signal)


* Constellation are shown in :numref:`fig_commtool2` and :numref:`fig_commtool3` for EbNo=5 and 10 respectively.  


.. _fig_commtool2:

.. figure:: img_commtoolbox/commtool2.png

   constellation for Eb/No = 5


.. _fig_commtool3:

.. figure:: img_commtoolbox/commtool3.png

   constellation for Eb/No = 10

BER plot for QPSK
=================

* In previous code, we get the BER value for EbNo = 5 and 10 by changing the value of EbNo manually for each run. 
* In below code, we put the EbNo in the 'for loop' and then plot the 'simulation BER' along with 'theoretical BER values'. 

.. code-block:: matlab
    :linenos:

    close all;
    clear all; 
    clc;

    max_num_bit = 1e7; % max number of bits to check before end of simulation
    max_num_error = 400;% max error to check before end of simulation


    % EbNo values to calculate BER
    EbNo_range = 1:10; 
    BER=zeros(1, length(EbNo_range)); % empty matrix to store BER



    % System model 
    % Modulator 
    qpsk_modulator = comm.QPSKModulator('BitInput', true); % constellation = 00, 01, 10, 11
    % AWGN noise
    AWGN = comm.AWGNChannel; 
    % Demodulator 
    qpsk_demodulator = comm.QPSKDemodulator('BitOutput', true); 
    % error rate
    error_rate = comm.ErrorRate; 



    for i = 1:length(EbNo_range)
        
        % received signal after AWGN channel
        AWGN.EbNo = EbNo_range(i) + 10*log10(2); % SNR

        num_err = 0; num_bits = 0; error_output=zeros(3,1); % intialize with 0
        bits_per_frame = 1024; % Frame size = n * bits/symbol where  n=integer
        while((num_err < max_num_error) && (num_bits < max_num_bit))
            % generate tx signal 
            s = randi([0 1], bits_per_frame, 1); %  'bits_per_frame' samples : column vector of 0 and 1
            tx_signal = qpsk_modulator.step(s); % generate tx signal
            rx_signal = AWGN.step(tx_signal); % add AWGN
            r = qpsk_demodulator.step(rx_signal); % demodulate signal
            % error_output has values [BER, number_errors, total_bit]
            error_output = error_rate.step(s, r); % calculate error
            num_err = error_output(2); % it's cumulative (as reset is not used for instance)
            num_bits  = error_output(3); % it's cumulative (as reset is not used for instance)
        end 
        
        clc;
        BER(i) = error_output(1) % store the BER values
        reset(error_rate); % reset the instance of comm.ErrorRate
    end 


    figure
    % theoretical results
    EbNo_linear = 10.^(EbNo_range/10);
    ber_theory = 0.5*erfc(sqrt(EbNo_linear));
    semilogy(EbNo_range, BER, '--b'); % simulation
    hold on
    semilogy(EbNo_range, ber_theory, '*r'); % theory
    xlabel('Eb/No (dB)');
    ylabel('BER'); 
    legend('simulation', 'theory');
    title('QPSK - BER vs EbNo');
    grid on


* Output at the terminal for above code, 

.. code-block:: text

    BER =

      Columns 1 through 9

       0.058593750000000   0.036051432291667   0.021330180921053   0.012390136718750   0.006024639423077   0.002529046474359   0.000741223908918   0.000177073889393   0.000035498636852

      Column 10

       0.000003599861765

.. _fig_commtool4:

.. figure:: img_commtoolbox/commtool4.png

   QPSK BER plot : simulation and theoretical


BER plot for QAM16
==================

* Below is the code for the BER plot for QAM16. Note that, we have used (NormalizationMethod', 'Average power') to normalize the power so that results will match with theoretical results. 

.. code-block:: matlab
    :linenos:
    :emphasize-lines: 52-54, 32, 17, 21, 29

    close all;
    clear all;
    clc;

    max_num_bit = 1e9; % max number of bits to check before end of simulation
    max_num_error = 10000;% max error to check before end of simulation


    % EbNo values to calculate BER
    EbNo_range = 1:10;
    BER=zeros(1, length(EbNo_range)); % empty matrix to store BER



    % System model
    % Modulator
    qam16_mod = comm.RectangularQAMModulator(16, 'BitInput', true, 'NormalizationMethod', 'Average power');
    % AWGN noise
    AWGN = comm.AWGNChannel;
    % Demodulator
    qam16_demodulator = comm.RectangularQAMDemodulator(16, 'BitOutput', true, 'NormalizationMethod', 'Average power');
    % error rate
    error_rate = comm.ErrorRate;



    for i = 1:length(EbNo_range)
        % received signal after AWGN channel
        AWGN.EbNo = EbNo_range(i) + 10*log10(4); % SNR

        num_err = 0; num_bits = 0; error_output=zeros(3,1); % intialize with 0
        bits_per_frame = 2400; % Frame size = n * bits/symbol where  n=integer
        while((num_err < max_num_error) && (num_bits < max_num_bit))
            % generate tx signal
            s = randi([0 1], bits_per_frame, 1); %  'bits_per_frame' samples : column vector of 0 and 1
            tx_signal = qam16_mod.step(s); % generate tx signal
            rx_signal = AWGN.step(tx_signal); % add AWGN
            r = qam16_demodulator.step(rx_signal); % demodulate signal
            % error_output has values [BER, number_errors, total_bit]
            error_output = error_rate.step(s, r); % calculate error
            num_err = error_output(2); % it's cumulative (as reset is not used for instance)
            num_bits  = error_output(3); % it's cumulative (as reset is not used for instance)
        end

        clc;
        BER(i) = error_output(1) % store the BER values
        reset(error_rate); % reset the instance of comm.ErrorRate
    end


    figure
    % theoretical results
    EbNo_linear = 10.^(EbNo_range/10);
    ber_theory = (3/8)*erfc(sqrt(EbNo_linear*4/10)); %M-QAM = 3/2n * erfc(sqrt(n*EbNo/10)); %%n=4 for QAM=16
    semilogy(EbNo_range, BER, '--b'); % simulation
    hold on
    semilogy(EbNo_range, ber_theory, '*r'); % theory
    xlabel('Eb/No (dB)');
    ylabel('BER');
    legend('simulation', 'theory');
    title('QAM16 - BER vs EbNo');
    grid on

* :numref:`fig_commtool5` shows the simulaiton result for above code,  

.. _fig_commtool5:

.. figure:: img_commtoolbox/commtool5.png

   QAM16 BER plot : simulation and theoretical



BER plot for QAM64
==================

* Code is exactly same as QAM16 expect '64' is used for Modulator and Demodulator. Also, M = 64 is used for theoretical plot. 

.. code-block:: matlab
    :linenos:
    :emphasize-lines: 55, 21, 17, 29

    close all;
    clear all;
    clc;

    max_num_bit = 1e9; % max number of bits to check before end of simulation
    max_num_error = 10000;% max error to check before end of simulation


    % EbNo values to calculate BER
    EbNo_range = 1:10;
    BER=zeros(1, length(EbNo_range)); % empty matrix to store BER



    % System model
    % Modulator
    qam64_mod = comm.RectangularQAMModulator(64, 'BitInput', true, 'NormalizationMethod', 'Average power');
    % AWGN noise
    AWGN = comm.AWGNChannel;
    % Demodulator
    qam64_demodulator = comm.RectangularQAMDemodulator(64, 'BitOutput', true, 'NormalizationMethod', 'Average power');
    % error rate
    error_rate = comm.ErrorRate;



    for i = 1:length(EbNo_range)
        % received signal after AWGN channel
        AWGN.EbNo = EbNo_range(i) + 10*log10(6); % SNR

        num_err = 0; num_bits = 0; error_output=zeros(3,1); % intialize with 0
        bits_per_frame = 2400; % Frame size = n * bits/symbol where  n=integer
        while((num_err < max_num_error) && (num_bits < max_num_bit))
            % generate tx signal
            s = randi([0 1], bits_per_frame, 1); %  'bits_per_frame' samples : column vector of 0 and 1
            tx_signal = qam64_mod.step(s); % generate tx signal
            rx_signal = AWGN.step(tx_signal); % add AWGN
            r = qam64_demodulator.step(rx_signal); % demodulate signal
            % error_output has values [BER, number_errors, total_bit]
            error_output = error_rate.step(s, r); % calculate error
            num_err = error_output(2); % it's cumulative (as reset is not used for instance)
            num_bits  = error_output(3); % it's cumulative (as reset is not used for instance)
        end

        clc;
        BER(i) = error_output(1) % store the BER values
        reset(error_rate); % reset the instance of comm.ErrorRate
    end


    figure
    % theoretical results
    EbNo_linear = 10.^(EbNo_range/10);
    % M-QAM => 2/k * (1-1/sqrt(M)) * erfc(sqrt(3*k*EbNo_linear/2/(M-1)))
    M=64; k=log2(M); 
    ber_theory=(2/k)*(1-1/sqrt(M))*erfc(sqrt(3*k*EbNo_linear/2/(M-1)));
        
    semilogy(EbNo_range, BER, '--b'); % simulation
    hold on
    semilogy(EbNo_range, ber_theory, '*r'); % theory
    xlabel('Eb/No (dB)');
    ylabel('BER'); ylim([0.001,0.25]);    
    legend('simulation', 'theory');
    title('QAM64 - BER vs EbNo');
    grid on

* :numref:`fig_commtool6` shows the simulaiton result for above code,  

.. _fig_commtool6:

.. figure:: img_commtoolbox/commtool6.png

   QAM64 BER plot : simulation and theoretical


Summary
=======

In this page, we used "communication toolbox" to plot the BER of QPSK, QAM16 and QAM64 system. 

