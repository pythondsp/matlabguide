Communication toolbox
=====================

Communication toolbox of MATLAB contains various algorithms e.g. channel coding and decoding along with various useful functionalities e.g. signal and noise generations etc. More details can be seen `here <https://in.mathworks.com/products/communications.html>`_.


.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: Contents:

   basic
   viterbi