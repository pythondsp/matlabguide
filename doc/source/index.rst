.. MATLAB Tutorial documentation master file, created by
   sphinx-quickstart on Mon Jan 30 20:05:58 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MATLAB R2020a
========================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   dsp/index
   commtoolbox/index


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

